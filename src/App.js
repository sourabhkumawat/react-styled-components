import React, { useState } from "react";
import "./App.css";
import styled, { ThemeProvider } from "styled-components";
import theme from "styled-theming";
const backgroundColor = theme("mode", {
  light: "#fafafa",
  dark: "#222",
});
const textColor = theme("mode", {
  light: "#000",
  dark: "#fff",
});
const Wrapper = styled.div`
  background-color: ${backgroundColor};
  color: ${textColor};
`;

function App() {
  const [mode, setMode] = useState("light");
  const changeMode = (mode) => {
    setMode(mode);
  };
  return (
    <div style={{ textAlign: "center" }}>
      <div>
        <div style={{ padding: 10 }}>
          <button onClick={() => changeMode("dark")}>Dark</button>
        </div>
        <div style={{ padding: 10 }}>
          <button onClick={() => changeMode("light")}>Light</button>
        </div>
      </div>
      <div style={{ padding: 100 }}>
        <ThemeProvider theme={{ mode: mode }}>
          <Wrapper>Hello World</Wrapper>
          <Wrapper>Switch between dark mode and light mode</Wrapper>
        </ThemeProvider>
      </div>
    </div>
  );
}

export default App;
